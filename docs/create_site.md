<div data-theme-toc="true"> </div>

# Creating a drupal site

You can create a drupal site from scratch by following a few simple steps. A drupal site is connected with an OpenShift project, which can have multiple drupal sites. Note that a project (and all drupal sites in it) has a single owner and SSO registration.

## From the OpenShift console

#### 1. Go to the [OpenShift console](https://drupal.cern.ch) page

Check on the left side menu that the **Administrator** option is selected (and **not** Developer).

#### 2. Create a new OpenShift project by clicking `Create Project` and fill in the information

![Screenshot](images/openshift_console-create_project.png)

---

* **Name**: Enter the name of your project.
* **Display Name** [optional]: Enter the display name of the project.
* **Description**: Enter a description of your site.

![Screenshot](images/openshift_console-create_project_fill.png)

---

#### 3. Click on `Installed Operators` on the left side menu

Make sure that you are in the correct `Project`. At first you will see a message "No Operators Found". Please wait a couple of minutes until the "DrupalSite Operator" appears as shown in the screenshot below. 

![Screenshot](images/openshift_console-installed_operators.png)

---

#### 4. Click on `Create Drupal website` on the latest DrupalSite Operator

![Screenshot](images/openshift_console-installed_operators_create_drupal_website.png)

---

#### 5. Click on `Create DrupalSite`

![Screenshot](images/openshift_console-create_drupalsite.png)

---

#### 6. Fill in the information to create a Drupalsite

* **Name**: Enter the name of the site.
* **Labels**: Ignore this field.
* **Publish**: If set to true, it makes the site accessible to the internet.
* **Disk Size**: Enter the maximum size of the site's files directory.
* **Site URL** [optional]: Enter the desired site URL. Must look like [mysite.web.cern.ch]() and not be used already by another site.
* **Web DAV Password** [optional]: Enter the desired password for WebDAV file access. A default is auto-generated if a value isn't given.

![Screenshot](images/openshift_console-create_drupalsite_fill.png)

---

#### 7. Click on `Environment` to expand the options panel with the default settings

Environment allows you to define alternative instances of the website with different content, that can be used to test/pilot site building, configuration, or updates.

* **Environment name**: Many branches of the website can exist in the same project for development and testing purposes. Set `production` for a public website, or any other value for branches.
* **Quality of Service class**: Request appropriate default configurations and resources for the requirements of your website.
* **Database Class**: Select the kind of database that the website needs.
* **Extra configs repo** [optional]: Provide additional settings to your website in a git repository.
* **Site Builder**: Ignore this field.
* **Init Clone From** [optional]: InitCloneFrom initializes this environment by cloning the specified DrupalSite (usually production), instead of installing an empty CERN-themed website.

![Screenshot](images/openshift_console-create_drupalsite_environment_fill.png)

---

#### 8. Click on `Version` to expand the options panel with the default settings

Version refers to the version and release of the CERN Drupal Distribution that will be deployed to serve this website. Changing this value triggers the website's update process.

* **Name**: Select the desired version of the branch of CERN Drupal Distribution.
* **Release Spec**: Specify the release version. The available release is: `RELEASE-2021.06.16T08-09-55Z`.

![Screenshot](images/openshift_console-create_drupalsite_version_fill.png)

---

#### 9. Click on `Create` button

![Screenshot](images/openshift_console-create_button.png)

---

Your drupal site is ready.

Keep in mind that related sites (production, development, test) can be grouped under a single project starting from [Step 3](#3-click-on-installed-operators-on-the-left-side-menu).

