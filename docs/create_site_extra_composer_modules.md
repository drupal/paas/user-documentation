<div data-theme-toc="true"> </div>

# Creating a drupal site with extra drupal modules

You can create a drupal site with extra drupal modules by following a few simple steps. 

For adding extra drupal modules to your site, you'll need to use:
- [Composer](https://getcomposer.org/): A tool for managing dependencies in PHP.
- A [GitLab repository](https://gitlab.cern.ch/help/user/project/repository/index.md): A place to store your code and change it with version control.

Note that this is the recommended workflow for adding drupal modules to your website, because you can fetch dependencies automatically and keep the website versioned.
This functionality will eventually be expanded to include site configuration as well. If you would like to add drupal modules without composer or git, please read [WebDAV file access]() guide. (available soon)

#### 1. Create a repository with `composer.json`

Every module that you want to install has to be included as a composer dependency. This can be done in two ways:
- by adding manually the right line in [composer.json](https://gitlab.cern.ch/drupal/paas/example-drupalsite-d8-extraconfig/-/blob/master/composer.json) under `require`.
- by running `composer require <module>`.

Afer, you need to create a repository and add the `composer.json` file, e.g. https://gitlab.cern.ch/drupal/paas/example-drupalsite-d8-extraconfig.git.

#### 2. Follow the [Creating a drupal site](https://drupal-community.web.cern.ch/t/creating-a-drupal-site/1121) guide till [Step 7](https://drupal-community.web.cern.ch/t/creating-a-drupal-site/1121#7-click-on-environment-to-expand-the-options-panel-with-the-default-settings-9)

#### 3. Fill the `Extra configs repo` field

![Screenshot](images/openshift_console-create_drupalsite_extraconfig_repo.png)

---

#### 4. Continue with [Step 8](https://drupal-community.web.cern.ch/t/creating-a-drupal-site/1121#8-click-on-version-to-expand-the-options-panel-with-the-default-settings-10) and [Step 9](https://drupal-community.web.cern.ch/t/creating-a-drupal-site/1121#9-click-on-create-button-11) of the [Creating a drupal site](https://drupal-community.web.cern.ch/t/creating-a-drupal-site/1121) guide

Your drupal site is ready.
