<div data-theme-toc="true"> </div>

# Accessing a migrated site

The default URL to access your migrated site is: `migrate-<SITENAME>.webtest.cern.ch`, where `SITENAME` is the name of your site in the old infrastructure.
This is the same name displayed on webservices.web.cern.ch to manage the website.

You can find your migrated sites in the OpenShift console by following a few simple steps.

#### 1. Go to the [OpenShift console](https://drupal.cern.ch) page

Check on the left side menu that the **Administrator** option is selected (and **not** Developer).

You will see a list of "projects". Each corresponds to 1 migrated website.

![Screenshot](images/openshift_console-projects_list.png)

---

#### 2. Click on `Installed Operators` on the left side menu

Make sure that you are in the correct `Project`. At first you will see a message "No Operators Found". Please wait a couple of minutes until the "DrupalSite Operator" appears as shown in the screenshot below.

![Screenshot](images/openshift_console-installed_operators.png)

---

#### 3. Click on `Create Drupal website` on the latest DrupalSite Operator

![Screenshot](images/openshift_console-installed_operators_create_drupal_website.png)

---

#### 4. View your sites

Related sites for different environments (production, development, test) are grouped under the same project.

![Screenshot](images/openshift_console-drupalsites_list.png)
